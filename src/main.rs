extern crate termion;

use std::io::Write;

use termion::{color, cursor};
use termion::event::Key;

mod graphics;
use graphics::Renderer;

mod game;
use game::{State, Game, UpdateResult};

/// Represent a walkable tile:
struct Node {
    /// Is there an obstacle here?
    obstacle: bool,
    /// Have searched this node yet?
    visited: bool,
    /// Distance to goal
    global_goal: f32,
    /// Distance to goal if we took the alternative path
    local_gloal: f32,
    /// x position of the node
    x: i32,
    /// y position of the node
    y: i32,

    /// list of connected neighbours
    neighbours: Vec<usize>,
    /// Node connecting to this one
    parent: usize,
}

pub struct Map {
    map: Box<[u8]>,
    width: u16,
    height: u16,
}

impl Map {
    pub fn new(width: u16, height: u16) -> Map {
        Map {
            map: vec![0; (width * height) as usize].into_boxed_slice(),
            width: width,
            height: height,
        }
    }
}

impl State for Map {
    fn render(&mut self, rdr: &mut Renderer) {
        let mut idx = 0;
        for y in 1..=self.height {
            for x in 1..=self.width {
                let color: &color::Color = match self.map[idx] {
                    0..=100   => &color::Rgb(90, 21, 234),
                    101..=200 => &color::Rgb(190, 121, 234),
                    201..=255 => &color::Rgb(255, 210, 234),
                    _ => &color::White,
                };
                write!(rdr, "{}{}{}{}", cursor::Goto(x, y), color::Fg(color), "█", color::Fg(color::Reset)).unwrap();
                idx += 1;
            }
        }
        write!(rdr, "{}{}Size are (w={}, h={}).{}", cursor::Goto(1, self.height + 1), color::Fg(color::AnsiValue(33)), self.width, self.height, color::Fg(color::Reset)).unwrap();
    }

    fn process(&mut self, evt: Key) -> UpdateResult {
        match evt {
            Key::Char('q') => UpdateResult::Pop,
            _ => UpdateResult::Nothing,
        }
    }
}

fn main() -> Result<(), std::io::Error> {
    let mut game = Game::new()?;
    let tsize = termion::terminal_size().ok();
    let twidth = tsize.map(|(w, _)| w);
    let theight = tsize.map(|(_, h)| h - 2);
    let map = Box::new(Map::new(twidth.unwrap_or(80), theight.unwrap_or(20) - 1));
    game.push(map);
    game.run()?;

    Ok(())
}
