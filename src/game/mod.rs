use std::collections::VecDeque;
use std::io::{self, Write};

use termion::{clear, style, cursor};
use termion::event::Key;

use super::Renderer;

#[allow(dead_code)]
pub enum UpdateResult {
    Push(Box<State>),
    Pop,
    Exit,
    Nothing,
}

pub trait State {
    fn render(&mut self, win: &mut Renderer);
    fn process(&mut self, evt: Key) -> UpdateResult;
}

pub struct Game {
    state: VecDeque<Box<State>>,
    quit: bool,
    window: Renderer,
}

impl Game {
    pub fn new() -> Result<Game, io::Error> {
        Ok(Game {
            state: VecDeque::new(),
            quit: false,
            window: Renderer::new()?,
        })
    }

    pub fn push(&mut self, state: Box<State>) {
        self.state.push_front(state);
    }

    fn exec(&mut self) -> Result<UpdateResult, io::Error> {
        if let Some(current) = self.state.front_mut() {
            current.render(&mut self.window);

            self.window.refresh()?;

            if let Some(input) = self.window.getch() {
                return Ok(current.process(input));
            } else {
                return Ok(UpdateResult::Exit);
            }
        }

        Ok(UpdateResult::Exit)
    }

    pub fn run(&mut self) -> Result<(), io::Error> {
        while !self.state.is_empty() && !self.quit {
            self.window.clear()?;

            match self.exec()? {
                UpdateResult::Exit => self.quit = true,
                UpdateResult::Pop => {
                    self.state.pop_front();
                },
                UpdateResult::Push(e) => {
                    self.state.push_front(e);
                },
                UpdateResult::Nothing => (),
            }
        }

        Ok(())
    }
}

impl Drop for Game {
    fn drop(&mut self) {
        write!(self.window, "{}{}{}", clear::All, style::Reset, cursor::Goto(1, 1)).unwrap();
    }
}
