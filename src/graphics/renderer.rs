use std::io::{Write, self};
use std::io::{stdin, stdout, Stdin, Stdout};

use termion::{clear, style, cursor};
use termion::event::Key;
use termion::input::{Keys, TermRead};
use termion::raw::IntoRawMode;
use termion::raw::RawTerminal;

pub struct Renderer {
    stdout: RawTerminal<Stdout>,
    stdin: Keys<Stdin>,
}

impl Renderer {
    pub fn new() -> io::Result<Renderer> {
        Ok(Renderer {
            stdout: stdout().into_raw_mode()?,
            stdin: stdin().keys(),
        })
    }

    pub fn clear(&mut self) -> io::Result<()> {
        write!(self.stdout, "{}{}{}", clear::All, style::Reset, cursor::Goto(1, 1))
    }

    pub fn refresh(&mut self) -> io::Result<()> {
        self.flush()
    }

    pub fn getch(&mut self) -> Option<Key> {
        self.stdin.next().map(|k| k.unwrap())
    }
}

impl Write for Renderer {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        self.stdout.write(buf)
    }

    fn flush(&mut self) -> io::Result<()> {
        self.stdout.flush()
    }
}
